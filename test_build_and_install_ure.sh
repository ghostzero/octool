#!/bin/bash
CURRENT_DIR=$(pwd)


# Install given opencog github repo
install_opencog_github_repo(){
local REPO="$1"
MESSAGE="Installing ${REPO}...." ; message
cd /tmp/
# cleaning up remnants from previous install failures, if any.
rm -rf master.tar.gz* ${REPO}-master
wget https://github.com/opencog/${REPO}/archive/master.tar.gz
tar -xvf master.tar.gz
wait
cd ${REPO}-master/
mkdir build
cd build/
cmake ..
wait
make -j$(nproc)
sudo make install
sudo ldconfig
cd /tmp/
rm -rf master.tar.gz ${REPO}-master/
#cd $CURRENT_DIR
wait
}



install_opencog_github_repo ure
