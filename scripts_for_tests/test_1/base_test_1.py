#from .SaveLoadTest import load_file, write_sorted_file

from opencog.type_constructors import *
from opencog.atomspace import AtomSpace
from opencog.utilities import initialize_opencog, finalize_opencog, load_file

atomSpace = AtomSpace()

set_default_atomspace(atomSpace)

link = SimilarityLink(ConceptNode("I can refer to this atom now"), ConceptNode("this one too"))

link.tv = TruthValue(0.5, 0.1)


node1 = ConceptNode("I can refer to this atom now")
node2 = ConceptNode("this one too")
link = atomSpace.add_link(types.SimilarityLink, [node1,node2])

atom = ConceptNode('one')
key = PredicateNode('av')
value = FloatValue([1.0, 2.0, 3.0])
atom.set_value(key, value)
atom.get_value(key)
atomSpace.add_atom(atom)

from opencog.utilities import initialize_opencog
from opencog.bindlink import execute_atom
from opencog.type_constructors import *

def add_link(atom1, atom2):
    return ListLink(atom1, atom2)

execute_atom(atomSpace,
    ExecutionOutputLink(
        GroundedSchemaNode("py: add_link"),
        ListLink(
            ConceptNode("one"),
            ConceptNode("two")
        )
    )
)

result = AnchorNode("1") == AnchorNode("1")
test1 = StringValue("QWE")
test2 = StringValue("QWE")
#state.AtomSpace.add_atom(test1)
#state.AtomSpace.add_atom(test2)

#exec(knowledgeBase.AtomSpace[ConceptNode("Main")].value)

# this checks universal rule engine. Very important.
from opencog.ure import BackwardChainer
from opencog.ure import ForwardChainer

#chainer = BackwardChainer(state.AtomSpace,
#                          rule_base,
#                          start_atom,
#                          trace_as=trace_atomspace)
#chainer.do_chain()
#results = chainer.get_results()

from opencog.type_constructors import *
from opencog.execute import execute_atom
from opencog.utilities import initialize_opencog, finalize_opencog
atomspace = AtomSpace()
set_default_atomspace(atomspace)
query_link = MeetLink(VariableNode("$test"), MemberLink(VariableNode("$test"), ConceptNode("Constant")))
result2 = execute_atom(atomspace, query_link)