#!/usr/bin/python3
#from .SaveLoadTest import load_file, write_sorted_file

from opencog.type_constructors import *
from opencog.atomspace import AtomSpace
from opencog.utilities import initialize_opencog, finalize_opencog, load_file

atomSpace = AtomSpace()

set_default_atomspace(atomSpace)

link = SimilarityLink(ConceptNode("I can refer to this atom now"), ConceptNode("this one too"))

link.tv = TruthValue(0.5, 0.1)


node1 = ConceptNode("I can refer to this atom now")
node2 = ConceptNode("this one too")
link = atomSpace.add_link(types.SimilarityLink, [node1,node2])

atom = ConceptNode('one')
key = PredicateNode('av')
value = FloatValue([1.0, 2.0, 3.0])
atom.set_value(key, value)
atom.get_value(key)
atomSpace.add_atom(atom)

from opencog.utilities import initialize_opencog
from opencog.bindlink import execute_atom
from opencog.type_constructors import *

def add_link(atom1, atom2):
    return ListLink(atom1, atom2)

execute_atom(atomSpace,
    ExecutionOutputLink(
        GroundedSchemaNode("py: add_link"),
        ListLink(
            ConceptNode("one"),
            ConceptNode("two")
        )
    )
)

result = AnchorNode("1") == AnchorNode("1")
test1 = StringValue("QWE")
test2 = StringValue("QWE")
#state.AtomSpace.add_atom(test1)
#state.AtomSpace.add_atom(test2)

#exec(knowledgeBase.AtomSpace[ConceptNode("Main")].value)

# this checks universal rule engine. Very important.
from opencog.ure import BackwardChainer
from opencog.ure import ForwardChainer

#chainer = BackwardChainer(state.AtomSpace,
#                          rule_base,
#                          start_atom,
#                          trace_as=trace_atomspace)
#chainer.do_chain()
#results = chainer.get_results()

from opencog.type_constructors import *
from opencog.execute import execute_atom
from opencog.utilities import initialize_opencog, finalize_opencog
atomspace = AtomSpace()
set_default_atomspace(atomspace)
query_link = MeetLink(VariableNode("$test"), MemberLink(VariableNode("$test"), ConceptNode("Constant")))
result2 = execute_atom(atomspace, query_link)



# that part is not working on my virtual machine

# setting and getting attention value as I know it is in implementation but is absent in cython bridge
# (https://wiki.opencog.org/w/Python   Values)

link.av = {"sti": 100, "lti": 200, "vlti": 5}


# that what I need new from implementation -- new type of node with bridge from c++ to python (https://wiki.opencog.org/w/Atom_types   Defining new Link Types) 
# as I understand it could be done in the same way as WordNode, because it is also custom.
# undestand that it could be hard. So if you will spend more than 40 hours -- it is ok. I will pay further with same rate 10 euro/hour
# just to have this feature, just notify me.

from opencog.type_constructors import *
import unittest

class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        test = ThingNode("Something")
        test2 = ThingNode("Something")
        self.assertFalse(test == test2) # I suppoce to create it different from other ConceptNode for example that
        # it should have default unique idenifier that will be for example in test.identifier field and exact value could be compared by hands .hold
        self.assertFalse(test.idenifier == test2.idenifier)
        self.assertTrue(test.hold == test2.hold)

if __name__ == '__main__':
    unittest.main()



# (https://wiki.opencog.org/w/Python   MOSES from Python)
# this feature has lower priority than custom type

from opencog.pymoses import moses
moses = moses()

input_data = [[0, 0, 0], [1, 1, 0], [1, 0, 1], [0, 1, 1]]
output = moses.run(input=input_data, python=True)
print (output[0].score) # Prints: 0
model = output[0].eval
model([0, 1])  # Returns: True
model([1, 1])  # Returns: False

output = moses.run(args="-H maj -c 1", python=True)
model = output[0].eval
model([0, 1, 0, 1, 0])  # Returns: False
model([1, 1, 0, 1, 0])  # Returns: True



finalize_opencog()
